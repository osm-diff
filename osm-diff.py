#! /usr/bin/python

import optparse, urllib2, xml.dom.minidom

parser = optparse.OptionParser()
parser.add_option("-n", "--node")

(options, args) = parser.parse_args()

url = "http://openstreetmap.com/api/0.5/node/%s/history" % options.node

data = urllib2.urlopen(url).read()
history = xml.dom.minidom.parseString(data)

history = history.childNodes[0]
assert history.nodeName == 'osm'

# FIXME make general
changes = sorted(history.getElementsByTagName('node'), key=lambda x: x.attributes['timestamp'].value)

last_node = changes[0]

print "This node was created by %s on %s" % (last_node.attributes['user'].value, last_node.attributes['timestamp'].value)

for this_node in changes[1:]:

    print "On %s user %s..." % (this_node.attributes['timestamp'].value, this_node.attributes['user'].value)

    # change in location
    last_loc = (last_node.attributes['lat'].value, last_node.attributes['lon'].value)
    this_loc = (this_node.attributes['lat'].value, this_node.attributes['lon'].value)
    if last_loc != this_loc:
        print "\tchanged the location to %s (old location %s)" % (last_loc, this_loc)

    last_tags = dict((x.attributes['k'].value, x.attributes['v'].value) for x in last_node.getElementsByTagName('tag'))
    this_tags = dict((x.attributes['k'].value, x.attributes['v'].value) for x in this_node.getElementsByTagName('tag'))

    # look at tags that have been added
    for tag in sorted(this_tags):
        if tag not in last_tags:
            print "\tadded tag %s=%s" % (tag, this_tags[tag])

    for tag in sorted(last_tags):
        if tag not in this_tags:
            print "\tdeleted tag %s=%s" % (tag, last_tags[tag])
        elif last_tags[tag] != this_tags[tag]:
            print "\tchanged tag %s from %s=%s to %s=%s" % (tag, tag,last_tags[tag], tag, this_tags[tag])
            

    last_node = this_node






